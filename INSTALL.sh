#!/usr/bin/env bash

set -e # Exit as soon as any line in the bash script fails

ROOTDIR=$( cd $(dirname $0) ; pwd -P ) # path to main TargetNGS directory

echo
exec &> >(tee -a  install.log)
exec 2>&1 # copies stderr onto stdout

# create a directory where all dependencies will be installed
cd $ROOTDIR

export PATH="$ROOTDIR/ext/miniconda/bin:$ROOTDIR/ext:$PATH"

minicondaPATH=$ROOTDIR/ext/miniconda
mkdir -p $minicondaPATH

# minimum tools version
numpy_VER=1.10.4
biopython_VER=1.70
bowtie2_VER=2.4.4
samtools_VER=1.13
Picard_VER=2.26.10
Prinseq_VER=0.20.4
cutadapt_VER=2.9

if [[ "$OSTYPE" == "darwin"* ]]
then
{
Picard_VER=2.26.10     
}
fi

install_biopython()
{
echo "--------------------------------------------------------------------------
                           installing Biopython 
--------------------------------------------------------------------------------
"
pip install biopython
echo "
------------------------------------------------------------------------------
                           Biopython installed
------------------------------------------------------------------------------
"
}

install_numpy()
{
echo "--------------------------------------------------------------------------
                           installing NumPy
--------------------------------------------------------------------------------
"
pip install numpy scipy
echo "
------------------------------------------------------------------------------
                           NumPy installed
------------------------------------------------------------------------------
"
}

install_picard()
{
echo "--------------------------------------------------------------------------
                           installing Picard Tool
--------------------------------------------------------------------------------
"
conda install --yes -p $minicondaPATH --no-deps -c bioconda picard=$Picard_VER openjdk
echo "
------------------------------------------------------------------------------
                           Picard Tool  installed
------------------------------------------------------------------------------
"
}

install_prinseq()
{
echo "--------------------------------------------------------------------------
                           installing PRINSEQ 
--------------------------------------------------------------------------------
"
conda install --yes -p $minicondaPATH --no-deps -c bioconda prinseq
echo "
------------------------------------------------------------------------------
                           PRINSEQ  installed
------------------------------------------------------------------------------
"
}

install_cutadapt()
{
echo "--------------------------------------------------------------------------
                           installing cutadapt
--------------------------------------------------------------------------------
"
pip install --prefix $minicondaPATH cutadapt
echo "
------------------------------------------------------------------------------
                           cutadaptinstalled
------------------------------------------------------------------------------
"
}

install_bowtie2()
{
echo "--------------------------------------------------------------------------
                           installing bowtie2
--------------------------------------------------------------------------------
"
if [[ "$OSTYPE" == "darwin"* ]]
then
{

  curl -k -L -o bowtie2-2.4.4-macos-x86_64.zip 'https://sourceforge.net/projects/bowtie-bio/files/bowtie2/2.4.4/bowtie2-2.4.4-macos-x86_64.zip'
  unzip bowtie2-2.4.4-macos-x86_64.zip
}
else
{
  curl -k -L -o bowtie2-2.4.4-linux-x86_64.zip 'https://sourceforge.net/projects/bowtie-bio/files/bowtie2/2.4.4/bowtie2-2.4.4-linux-x86_64.zip'
  unzip bowtie2-2.4.4-linux-x86_64.zip
}
fi
mkdir -p $minicondaPATH/bin/
cp -f bowtie2*/bowtie2* $minicondaPATH/bin/
rm -rf bowtie2-*

echo "
------------------------------------------------------------------------------
                           bowtie2 installed
------------------------------------------------------------------------------
"
}

install_samtools()
{
echo "--------------------------------------------------------------------------
                           Downloading samtools v$samtools_VER
--------------------------------------------------------------------------------
"
conda install --yes -p $minicondaPATH -c bioconda samtools=$samtools_VER
echo "
--------------------------------------------------------------------------------
                           samtools v$samtools_VER installed
--------------------------------------------------------------------------------
"
}

install_miniconda()
{
echo "--------------------------------------------------------------------------
                           downloading miniconda
--------------------------------------------------------------------------------
"
if [[ "$OSTYPE" == "darwin"* ]]
then
{

  curl -k -A "Mozilla/5.0" -L -o miniconda.sh https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh

}
else
{  

  curl -k -A "Mozilla/5.0" -L -o miniconda.sh https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
}
fi

chmod +x miniconda.sh
./miniconda.sh -b -p $minicondaPATH -f
export PATH=$ROOTDIR/ext/miniconda/bin:$PATH

}

checkSystemInstallation()
{
    IFS=:
    for d in $PATH; do
      if test -x "$d/$1"; then return 0; fi
    done
    return 1
}

checkLocalInstallation()
{
    IFS=:
    for d in $minicondaPATH/bin; do
      if test -x "$d/$1"; then return 0; fi
    done
    return 1
}

checkPythonPackage()
{
	python -c "import $1; " 
	return $?
}

versionStr() {
  echo "$@" | awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }';
}


####### MAIN #######


if ( checkSystemInstallation conda )
then
	conda_installed_VER=`conda --version 2>&1|perl -nle 'print $& if m{(\d+\.\d+\.\d+)}'`;
  	echo " - found conda $conda_installed_VER"
	echo "conda create --prefix $minicondaPATH -y"
	conda create --prefix $minicondaPATH -y
else
 	install_miniconda
fi


if ( checkSystemInstallation bowtie2 )
then
	bowtie_installed_VER=`bowtie2 --version | grep bowtie | perl -nle 'print $1 if m{version (\d+\.\d+\.\d+)}'`;
	if [ $(versionStr $bowtie_installed_VER) -ge $(versionStr $bowtie2_VER) ]
	then
		echo " - found bowtie2 $bowtie_installed_VER"
	else
		install_bowtie2
	fi
else
	install_bowtie2	
fi

if ( checkSystemInstallation samtools )
then
	samtools_installed_VER=`samtools 2>&1| grep 'Version'|perl -nle 'print $1 if m{Version: (\d+\.\d+.\d+)}'`;
	if [ -z "$samtools_installed_VER" ]
	then 
		samtools_installed_VER=`samtools 2>&1| grep 'Version'|perl -nle 'print $1 if m{Version: (\d+\.\d+)}'`; 
	fi
	if [ $(versionStr $samtools_installed_VER) -ge $(versionStr $samtools_VER) ]	
	then
		echo " - found samtools $samtools_installed_VER"
	else
		install_samtools
	fi
else
  install_samtools
fi

if ( checkPythonPackage numpy )
then
	numpy_installed_VER=`python -c "import numpy; print(numpy.__version__)" || true`;
	if [ $(versionStr $numpy_installed_VER) -ge $(versionStr $numpy_VER) ]
	then
		echo " - found numpy $numpy_installed_VER"
	else
		install_numpy
	fi
else
	install_numpy
fi

if ( checkPythonPackage Bio )
then
	biopython_installed_VER=`python -c 'import Bio; print(Bio.__version__)' || true`;
	if [ $(versionStr $biopython_installed_VER) -ge $(versionStr $biopython_VER) ]
	then
		echo " - found biopython $biopython_installed_VER"
	else
		install_biopython
	fi
else
	install_biopython
fi

#if ( checkSystemInstallation picard )
#then
#	picard_installed_VER=`picard MarkDuplicates --version 2>&1 | perl -nle 'print $& if m{\d+.\d+\.\d+}'`;
#	if [ $(versionStr $picard_installed_VER) -ge $(versionStr $Picard_VER) ]
#	then
#		echo " - found picard $picard_installed_VER"
#	else
#		install_picard
#	fi
#else
#	install_picard
#fi


if ( checkSystemInstallation prinseq-lite.pl )
then
        PRINSEQ_installed_VER=`prinseq-lite.pl -version | perl -nle 'print $& if m{\d+\.\d+\.\d+}'`;
	if [ $(versionStr $PRINSEQ_installed_VER) -ge $(versionStr $Prinseq_VER) ]
        then
                echo " - found PRINSEQ $PRINSEQ_installed_VER"
        else
                install_prinseq
        fi
else
        install_prinseq
fi

if ( checkSystemInstallation cutadapt )
then
	cutadapt_installed_VER=`cutadapt --version`;
	if [ $(versionStr $cutadapt_installed_VER) -ge $(versionStr $cutadapt_VER) ]
        then
                echo " - found cutadapt $cutadapt_installed_VER"
        else
                install_cutadapt
        fi
else
	install_cutadapt
fi

conda clean -y -a

echo "
==============================================
	Installation  successfully
==============================================

	./consensus_fasta.py  -h

for usage.
Read the README for more information!
Thanks!
";
