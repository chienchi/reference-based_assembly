#!/usr/bin/env python3

# By Jason Ladner

###!!! Requires installations of samtools and bowtie2, both of which must reside within your $PATH

from __future__ import division
import sys, optparse, os, glob, re, shutil, errno

dir_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0,dir_path + "/ext/miniconda/lib/python2.7/site-packages");
ext_path = dir_path + "/ext/miniconda/bin"
os.environ["PATH"] = ext_path + os.pathsep + os.environ["PATH"] 

from subprocess import Popen, PIPE
from Bio.Seq import Seq
import numpy as np
import scipy.stats as stats
import copy
from collections import defaultdict

try:
    from Bio.Alphabet import generic_dna
except ImportError:
    generic_dna = None

__version__ = "5.10.0"

def main():

    #To parse command line
    usage = "usage: %prog [options] bam1 [bam2 ...]"
    p = optparse.OptionParser(usage,version= "Version: %s" % __version__ )
    
    #Input/output files
    p.add_option('-i', '--input', help='Input file for running multiple samples from fastqs [None]')
    p.add_option('-u', '--unpaired', help='Fastq file with unpaired reads to be mapped to reference [None]')
    p.add_option('-1', '--paired1', help='Fastq file with read 1 of paired reads [None]')
    p.add_option('-2', '--paired2', help='Fastq file with read 2 of paired reads [None]')
    p.add_option('-r', '--ref', help='Fasta file that was used as reference in mapping. [None, Required]')
    p.add_option('-o', '--out', default='new_consensus', help='Base name for output files. Output files will be written to the current working directory. [new_consensus]')
    p.add_option('--comp',default=False, action='store_true', help="output pile up with nucleotide composition")
    p.add_option('--varlog',default=False, action='store_true', help="output changelog for any variants")
    p.add_option('--noVcf',default=False, action='store_true', help="do not output vcf file")
    p.add_option('--bed',help="only build from bed file covered regions, others will be masked with 'n'")
    p.add_option('-s', '--sam', help='Optional starting place [None]')
    p.add_option('-b', '--bam', help='Optional starting place [None]')
    p.add_option('-p', '--pile', help='Optional starting place [None]')
    

    
    #General
    p.add_option('--recursThresh', type='int', default=50000, help='Minimum level of coverage required to change consensus. [3000]')
    p.add_option('--temp', help='Name for working directory that will hold internediate files. This is not currently being cleaned [based on -o]')
    p.add_option('--procs', type='int', default=2, help='Number of processors to use in multi-threaded portions [2]')
    p.add_option('--Nbeg', type='int', default=0, help='# of Ns to add to the beginning of each contig [0]')
    p.add_option('--Nend', type='int', default=0, help='# of Ns to add to the beginning of each contig [0]')    
    
    #For samtools
    p.add_option('--mapQ', type='int', default=10, help='Minimum mapping quality for a read to be used in the pileup generation [10]')
    p.add_option('--maxCov', type='int', default=300, help='Max per base coverage to be used in the pileup generation [300]')
    #This step allows for a large amount of time saved when a small number of the reads actually map to the reference. Should probably be used for viral samples, but not bacterial.
    p.add_option('--NOonlyMapped', default=False, action='store_true', help='Use this flag to turn off the part of the script that creates a smaller sam with only mapped reads prior to sorting')
    
    #For bowtie2
    p.add_option('--nceil', default='0,0.3',  help='Function to supply as --n-ceil parameter in bowtie2. [0,0.3]')
    p.add_option('--minIns', type='int', default=0, help='Minimum insert size for a pair to be concordant [0]')
    p.add_option('--maxIns', type='int', default=500, help='Maximum insert size for a pair to be concordant [500]')
    
    #Make new consensus
    p.add_option('--propThresh', type='float', default=0.5, help='Requires that a greater proportion of the reads (than this threshold) must support an alternative base for the consensus to be changed. [0.5]')
    p.add_option('--hetpropThresh', type='float', default=0.2, help='Requires that a greater proportion of the reads (than this threshold) must support an alternative base for the consensus to be changed to IUPAC ambiguous nucleotide code. [0.2]')
    p.add_option('--INDELpropThresh', type='float', default=0.5, help='Requires that a greater proportion of the reads (than this threshold) must support an INDELs for the consensus to be changed. [0.5]')
    p.add_option('--covThresh', type='int', default=5, help='Minimum level of coverage required to avoid an N in the consensus. [5]')
    p.add_option('--offset', type='int', default=33, help='Base quality offset used in the pileup. I believe the default in samtools is Sanger(33) [33]')
    p.add_option('--baseQual', type='int', default=20, help='Minimum base quality for a base to be used in the evalution of the consensus. [20]')
    p.add_option('--minHomopolymer', type='int', default=4, help='Minimum length of a poly[A,T,C,G] stretch [4]')
    p.add_option('--filterHomopolymer', default=False, action='store_true', help='filter the INDELs variants in homopolymer region based on minHomopolymer and hpmINDELpropThresh')
    p.add_option('--hpmINDELpropThresh', type='float', default=0.8, help='Requires that a greater proportion of the reads in homopolymer region (than this threshold) must support an INDELs for the consensus to be changed. [0.8]')
    p.add_option('--filterStrandBias', default=False, action='store_true', help='filter the SNP variants based on both Fisher Exact test --fisherScore and odds ratio score --strandOddsRation')
    p.add_option('--fisherScore', default=60, type='int', help='Fisher Exact test Phred scaled p-value threshhold [60]')
    p.add_option('--strandOddsRatio', default=3, type='int', help='The threshhold for strand bias estimated by the symmetric odds ratio test, threshhold [3]')
    
    #Cutadapt and Prinseq
    #How to call programs
    p.add_option('--ca', default='cutadapt', help='How to call cutadapt. Need to use version >=1.2 [cutadapt]')
    p.add_option('--ps', default='prinseq-lite.pl', help='How to call prinseq. [prinseq-lite.pl]')
    p.add_option('--pc', default='picard', help='How to call picard. [picard]')
#    p.add_option('--tr', default='~/programs/trimmomatic-0.33.jar', help='How to call trimmomatic. [~/programs/trimmomatic-0.33.jar]')
    p.add_option('--i1', default='GATCGGAAGAGCACACGTCTGAACTCCAGTCAC', help="Illumina adaptor to be trimmed from the 3' end of R1. [GATCGGAAGAGCACACGTCTGAACTCCAGTCAC]")
    p.add_option('--i2', default='AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT', help='Reverse complement of the Illumina adaptor that will be trimmed from the 3; end of R2 [AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT')
    p.add_option('-l', '--minLength', default=70, type='int', help='Minimum length to keep (after primer clipping) [70]')
    p.add_option('--trimQual', default=30, type='int', help='Minimum quality to keep at 3 prime end f seqs [30]')
    p.add_option('--trimType', default='min', help='Type of quality score calculation to use. Allowed options are min, mean, max and sum [min]')
    p.add_option('--trimWin', default=5, type='int', help='Size of window to use for quality trimming [5]')
    p.add_option('-q', '--meanQual', default=20, type='int', help='Minimum mean quality for a read to be kept [20]')
    p.add_option('--sispa', help='For Sispa , use GCCGGAGCTCTGCAGATATC. Add GCCGGAGCTCTGCAGATATCGGCCATTATGGCCGGG and GCCGGAGCTCTGCAGATATCAAAAAAAAAAAAA for Sispa RACE. For kikwit amplicons, add TGTAAAACGACGGCCAGT. Sispa adapter to remove from the seqs. Rev comp of this sequence will be removed from the 3 prime end [None]')
    #Use this flag if you are starting with reads, but you don't want to run cutadapt
    p.add_option('--indexFilt', type='int', help='Use this to specify the quality cutoff for index filtering. Currently only supported in combination with -i [None].')
    p.add_option('--useAnomPairs', default=False, action='store_true', help='Use this flag to use anomalous read pairs in pileup [False]')
    p.add_option('--disableBAQ', default=False, action='store_true', help='Disable BAQ calculation in pileup [False]')
    #Number of bases to hard clip from the beginning of R1, end of R2, to get rid of any potential random hexamers
    p.add_option('--hc', default=6, type='int', help='Number of bases to be hard clipped from the beginning of R1 and end of R2. This will be done with prinseq [6]')

    #To turn off different parts of the pipeline
#    p.add_option('--NOtrimmomatic', default=False, action='store_true', help='Use this flag to turn off the part of the script that runs trimmomatic')
    p.add_option('--NOcutadapt', default=False, action='store_true', help='Use this flag to turn off the part of the script that runs cutadapt')
    p.add_option('--NOprinseq', default=False, action='store_true', help='Use this flag to turn off the part of the script that runs prinseq')
    p.add_option('--NOpcrDedup', default=False, action='store_true', help='Use this flag to turn off the part of the script that runs PCR deduplication')


    opts, args = p.parse_args()

    opts.index1=''
    opts.index2=''
    
    #Increase recursion limit before starting script
    sys.setrecursionlimit(opts.recursThresh)
    
    #To allow for trimming multiple sispa like sequences (trimmed from both sides of reads), this function replaces the sequences with the string that will go into the cutadapt command
    if opts.sispa:
        opts.sispa=make_sispa_str(opts.sispa)
    
    if len(sys.argv) == 1:
        p.print_help()
        sys.exit(1)

    #If providing multiple bams as args. Output names will be taken from bam names
    if args:
        for bamfile in args:
            opts.bam=bamfile
            opts.out='.'.join(bamfile.split('.')[:-1])
            opts.temp=None
            run_iteration(opts)


    #If you are running multiple samples from fastqs
    elif opts.input:
        finin = open(opts.input, 'r')
        start_ref=opts.ref
        for line in finin:
            #This resting of the ref is important if you are adding Ns onto the ends of the ref
            opts.ref=start_ref
            cols=line.strip().split('\t')
            opts.out=cols[0]
            print(cols[1])
            opts.paired1=','.join(sorted(glob.glob('%s*R1*' % cols[1])))
            opts.paired2=','.join(sorted(glob.glob('%s*R2*' % cols[1])))
            if opts.indexFilt:
                opts.index1=','.join(sorted(glob.glob('%s*I1*' % cols[1])))
                opts.index2=','.join(sorted(glob.glob('%s*I2*' % cols[1])))
            if len(cols)>2: opts.ref=cols[2]
            print(opts.paired1, opts.paired2, opts.ref)
            run_iteration(opts)

    #If you are running multiple bam files
    elif ',' in opts.out:
        out_list=opts.out.split(',')
        if opts.bam:
            bam_list=opts.bam.split(',')
            if len(bam_list) == len(out_list):
                for i in range(len(out_list)):
                    opts.out=out_list[i]
                    opts.bam=bam_list[i]
                    opts.temp=None
                    run_iteration(opts)
            else: print('Must provide the same number of input files and output names!!!!')
    
    #If you are running just one bam file or something else
    else: run_iteration(opts)

###-----------------End of main()--------------------------->>>

def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return izip(a, b)

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise
            
def make_sispa_str(seqs_comma):
    cut_str=''
    for seq in seqs_comma.split(','):
        cut_str+=' -g sispaF=%s -a sispaR=%s ' % (seq, rev_comp(seq))
    return cut_str

def concat_fastqs(fastq_str, newname):
    fastqs=fastq_str.split(',')
    cmd='zcat %s >%s' % (' '.join(fastqs), newname)
    print(cmd)
    concat=Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
    concat.wait()
    return newname

def convert_bed_to_amplicon_dict(bed_input):
    ## convert bed file to amplicon region dictionary
    input_bed = bed_input
    amplicon=defaultdict(dict)
    cmd = 'grep -v alt %s | paste - - | cut -f 1,2,3,4,8,9 | sed -e "s/_LEFT//g" -e "s/_RIGHT//g" ' % (input_bed)
    proc = Popen(cmd,shell=True,stdout=PIPE) 
    outs, errs = proc.communicate()
    if proc.returncode == 0:
        outs = outs.splitlines()
    else:
        print("Failed %d %s %s" % (proc.returncode, outs, errs))
   
    for i in range(len(outs)):
        chrom, fstart, fend, primer_id, rstart, rend = outs[i].decode().rstrip().split("\t")
        for pos in range(int(fend)+1,int(rstart)+1):
            if pos in amplicon[chrom]:
                amplicon[chrom][pos] += 1
            else:
                amplicon[chrom][pos] = 1  
    return amplicon

def run_iteration(opts):
    #Concatenate fastqs, if multiple
    #And set to absolute path
    if opts.paired1:
        if ',' in opts.paired1:
            opts.paired1=concat_fastqs(opts.paired1, '%s_R1.fastq' % (opts.out))
        opts.paired1=os.path.abspath(opts.paired1)
    if opts.paired2:
        if ',' in opts.paired2:
            opts.paired2=concat_fastqs(opts.paired2, '%s_R2.fastq' % (opts.out))
        opts.paired2=os.path.abspath(opts.paired2)
    
    if opts.index1:
        if ',' in opts.index1:
            opts.index1=concat_fastqs(opts.index1, '%s_I1.fastq' % (opts.out))
        opts.index1=os.path.abspath(opts.index1)
    if opts.index2:
        if ',' in opts.index2:
            opts.index2=concat_fastqs(opts.index2, '%s_I2.fastq' % (opts.out))
        opts.index2=os.path.abspath(opts.index2)
    
    #Index filter, if requested
    if opts.indexFilt:
        opts.paired1, opts.paired2 = filter_index(opts)
        opts.paired1=os.path.abspath(opts.paired1)
        opts.paired2=os.path.abspath(opts.paired2)

    #Change all filenames to their absolute path versions
    opts.ref=os.path.abspath(opts.ref)
    if opts.unpaired: opts.unpaired=os.path.abspath(opts.unpaired)
    if opts.pile: opts.pile=os.path.abspath(opts.pile)
    if opts.sam: opts.sam=os.path.abspath(opts.sam)
    if opts.bam: opts.bam=os.path.abspath(opts.bam)
    
    #If no name was provied for the temp directory, make a name for it based on the -o flag
    if not opts.temp: opts.temp='%s_interfiles' % opts.out

    #Save current working directory
    opts.startDir=os.getcwd()
    
    #Create consensus
    create_consensus(opts)
    
    #Added this line so that if running multiple analyses at once
    opts.temp=None


#Index filter scripts--------------------

def filter_index(opts):
    
    #Open new output files
    fout_r1 = open('%s_R1_Q%d.fastq' % (opts.out, opts.indexFilt), 'w')
    fout_r2 = open('%s_R2_Q%d.fastq' % (opts.out, opts.indexFilt), 'w')
#    fout_i1 = open('%s_Q%d.fastq' % (opts.I1.split('.')[0], opts.minQual), 'w')
#    fout_i2 = open('%s_Q%d.fastq' % (opts.I2.split('.')[0], opts.minQual), 'w')
    #Open input fastq files
    fin_r1 = open(opts.paired1, 'r')
    fin_r2 = open(opts.paired2, 'r')
    fin_i1 = open(opts.index1, 'r')
    fin_i2 = open(opts.index2, 'r')
    
#    good_index_count=0
    while 1:
        first_mate=[]
        second_mate=[]
        first_index=[]
        second_index=[]
        #Extract a single sequence from each read file
        for i in range(4):
            first_mate.append(fin_r1.readline())
            second_mate.append(fin_r2.readline())
            first_index.append(fin_i1.readline())
            second_index.append(fin_i2.readline())
            
        if first_mate[0]=='': 
            #Close all open files
            fout_r1.close()
            fout_r2.close()
#            fout_i1.close()
#            fout_i2.close()
            fin_r1.close()
            fin_r2.close()
            fin_i1.close()
            fin_i2.close()
            return '%s_R1_Q%d.fastq' % (opts.out, opts.indexFilt), '%s_R2_Q%d.fastq' % (opts.out, opts.indexFilt)
        else: 
            if good_qual(first_index[3], opts.indexFilt, opts.offset) and good_qual(second_index[3], opts.indexFilt, opts.offset):
#                good_index_count+=1
                fout_r1.writelines(first_mate)
                fout_r2.writelines(second_mate)
#                fout_i1.writelines(first_index)
#                fout_i2.writelines(second_index)
#    print 'Number of good pairs remaining: %d' % (good_index_count)

    return '%s_R1_Q%d.fastq' % (opts.out, opts.indexFilt), '%s_R2_Q%d.fastq' % (opts.out, opts.indexFilt)

def good_qual(index_quals, min_qual, offset):
    index_quals=[ord(x)-offset for x in index_quals.strip()]
    if np.average(index_quals)>=min_qual: return True
    else: return False
    
###---------End of index filter scripts---------------

def quality_filter(bases, quals, opts):
    new_bases=[]
    new_quals=[]
    for index in range(len(bases)):
        if ord(quals[index])-opts.offset >= opts.baseQual:
            new_bases.append(bases[index])
            new_quals.append(quals[index])
    return new_bases, new_quals


def create_consensus(opts):

    #Create temporary working direcory and move to that directory
    mkdir_p(opts.temp)
    os.chdir(opts.temp)

    if opts.Nbeg or opts.Nend:
        Ns_ref=add_Ns(opts.ref, opts.Nbeg, opts.Nend)
        opts.ref=Ns_ref
    else: Ns_ref='fake_name_that_hopefully_wont_match_a_real_file'

    
    #If you are staring with reads that need to be mapped
    if not opts.pile and not opts.bam and not opts.sam:
        
        #Not adding trimmomatic just now
        #Run Trimmomatic, unless NOtrimmomatic flag was thrown
#        if not opts.NOtrimmomatic:
            
        
        #Run Cutadapt, unless NOcutadapt flag was thrown
        if not opts.NOcutadapt:
            #Run cutadapt
            one_cut, one_info_file = run_cutadapt(opts.paired1, opts.i1, opts)
            two_cut, two_info_file = run_cutadapt(opts.paired2, opts.i2, opts)

            #Delete random primers associated with sispa adapters
#            if opts.num:
#                one_cut_trim=trim_fastq(one_cut, one_info_file, opts)
#                two_cut_trim=trim_fastq(two_cut, two_info_file, opts)
#                delete_files(one_cut, two_cut, one_info_file, two_info_file)
#                one_cut=one_cut_trim
#                two_cut=two_cut_trim

            #Make new fastqs with just reads that are still paired. Delete intermediate files
            opts.paired1, opts.paired2 = paired_sort(one_cut, two_cut)
            opts.paired1=os.path.abspath(opts.paired1)
            opts.paired2=os.path.abspath(opts.paired2)
            delete_files(one_cut, two_cut)

        #Hard clip a defined number of bases from the beginning of R1 and the end of R2, if specified
        #This is to make sure to get rid of any random mer incorporated during library prep
        if opts.hc:
            opts.paired1, opts.paired2 = hard_clip(opts)

        #Run prinseq, unless NOprinseq flag was thrown
        if not opts.NOprinseq:
            opts.paired1, opts.paired2 = paired_prinseq(opts)
        
        #Add Ns to the beginning and ends of contigs if specified. Ns_ref variable makes it easy to remove this temporary file at the end
        #First alignment of reads to reference with bowtie2
        sam = bowtie2_index_align(opts.ref, opts)
    
    if not opts.pile and not opts.bam:
        if opts.sam: sam=opts.sam
        
        if opts.NOonlyMapped:
            bam, pileup = make_sort_bam_pileup(sam, opts.ref, opts)
            delete_files(sam)
        else:
            mapped_sam = sam_just_mapped(sam)
            bam, pileup = make_sort_bam_pileup(mapped_sam, opts.ref, opts)
            num_changes, current_consensus, current_consensus_w_ambiguous = make_consensus(pileup, opts.ref, opts)    
            sam_to_bam(sam, opts)
            delete_files(mapped_sam, sam)
            
    elif not opts.pile and opts.bam:
        bam, pileup = sort_bam_pileup(opts.bam, opts.ref, opts)
        num_changes, current_consensus, current_consensus_w_ambiguous = make_consensus(pileup, opts.ref, opts)    

    elif opts.pile:
        pileup=opts.pile
        num_changes, current_consensus, current_consensus_w_ambiguous = make_consensus(pileup, opts.ref, opts)    
        
    #Create final files, leave working directory and clean it
    new_cons_fasta = '%s/%s.fasta' % (opts.startDir, opts.out)
    new_cons_w_ambiguous_fasta = '%s/%s_w_ambiguous.fasta' % (opts.startDir, opts.out)
    os.rename(current_consensus, new_cons_fasta)
    os.rename('change_log.txt', '%s/%s.changelog' % (opts.startDir, opts.out))
    os.rename('gaps.txt', '%s/%s.gaps' % (opts.startDir, opts.out))     
    if opts.comp:
        os.rename('composition_log.txt', '%s/%s.compositionlog' % (opts.startDir, opts.out))
    if opts.varlog:
        os.rename('var_log.txt', '%s/%s.varlog' % (opts.startDir, opts.out))  
    if current_consensus_w_ambiguous:
        os.rename(current_consensus_w_ambiguous, new_cons_w_ambiguous_fasta)
    if not opts.noVcf:
        os.rename('change_log.vcf', '%s/%s.vcf' % (opts.startDir, opts.out))
    
    for file in ['ref_index.1.bt2', 'ref_index.2.bt2', 'ref_index.3.bt2', 'ref_index.4.bt2', 'ref_index.rev.1.bt2', 'ref_index.rev.2.bt2',]: 
        if os.path.isfile(file):
            os.remove(file)
    os.chdir(opts.startDir)
#    os.rmdir(opts.temp)

###-------------Start of functions to add and remove Ns from reference---------

def add_Ns(fasta, num_beg, num_end):
    new_fasta_name='%s_%dNbeg_%dNend.fasta' % ('.'.join(fasta.split('.')[:-1]), num_beg, num_end)
    names, seqs = read_fasta_lists_simple_names(fasta)
    seqs=[num_beg*'N'+x+num_end*'N' for x in seqs]
    write_fasta(names, seqs, new_fasta_name)
    return new_fasta_name

def trim_Ns(fasta):
    names, seqs = read_fasta_lists_simple_names(fasta)
    seqs=[x.strip('N') for x in seqs]
    write_fasta(names, seqs, fasta)
    
    
###-------------End of functions to add and remove Ns from reference---------

def hard_clip(opts):
    #Clip the specified # bases from the beginning of R1
    out_name1 = '%s_trim%dbeg' % ('.'.join(opts.paired1.split('/')[-1].split('.')[:-1]), opts.hc)
    cmd='%s -fastq %s -out_good %s -out_bad null -trim_left %d' % (opts.ps, opts.paired1, out_name1, opts.hc)
    print(cmd)
    ps=Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
    ps.wait()
    for line in ps.stderr:
        print(line.rstrip())
    
    #Clip the specified # bases from the end of R2
    out_name2 = '%s_trim%dend' % ('.'.join(opts.paired2.split('/')[-1].split('.')[:-1]), opts.hc)
    cmd='%s -fastq %s -out_good %s -out_bad null -trim_right %d' % (opts.ps, opts.paired2, out_name2, opts.hc)
    print(cmd)
    ps=Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
    ps.wait()
    for line in ps.stderr:
        print(line.rstrip())

    return os.path.abspath("%s.fastq" % out_name1), os.path.abspath("%s.fastq" % out_name2)

###----------Start of Cutadapt and Prinseq functions----------------------------------------

def rev_comp(seq):
    if generic_dna:
        dna = Seq(seq, generic_dna)
    else:
        dna = Seq(seq)
    return str(dna.reverse_complement())

#Use this to delete some files, given that those files exist    
def delete_files(*done_files):
    for to_remove in done_files:
        if os.path.isfile(to_remove):
            os.remove(to_remove)

def run_cutadapt(fastq, ill_adapt, opts):
    if fastq.endswith('.gz'): out_name = '%s_cutadapt.fastq' % '.'.join(fastq.split('.')[:-2])
    else: out_name = '%s_cutadapt.fastq' % '.'.join(fastq.split('.')[:-1])
    info_file_name =  '%s_ca_info.txt' % '.'.join(fastq.split('.')[:-1])
    if opts.sispa: cmd='%s -a illumina=%s %s -o %s -m %d --info-file %s --times 3 --match-read-wildcards %s --trim-n' % (opts.ca, ill_adapt, opts.sispa, out_name, opts.minLength, info_file_name , fastq)
    else: cmd='%s -a illumina=%s -o %s -m %d --info-file %s --times 3 --match-read-wildcards %s --trim-n' % (opts.ca, ill_adapt, out_name, opts.minLength, info_file_name , fastq)
    print(cmd)
    cutit=Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
    cutit.wait()
    for line in cutit.stdout:
        print(line.rstrip())
    return out_name, info_file_name

###-----Functions for removing random primers after running cutadapt with the sispa seqs

def trim_fastq(fq, ifile, opts):

    info_dict=make_info_dict(ifile)
    
    fin=open(fq, 'r')
    fout=open('%s_trimmed' % fq, 'w')
    
    while 1:
        this_read = []
        for i in range(4):
            this_read.append(fin.readline())
        if this_read[0]=='': 
            #Close all open files
            fout.close()
            fin.close()
            return '%s_trimmed' % fq
        else:
            name=this_read[0].strip()[1:]
            if name in info_dict: 
                if 'sispaF' in info_dict[name]:
                    this_read[1]=this_read[1][opts.num:]
                    this_read[3]=this_read[3][opts.num:]
                if 'sispaR' in info_dict[name]:
                    this_read[1]='%s\n' % this_read[1].strip()[-opts.num:]
                    this_read[3]='%s\n' % this_read[3].strip()[-opts.num:]
                fout.writelines(this_read)
            else: fout.writelines(this_read)

    
def make_info_dict(info_file):
    idict={}
    fin=open(info_file, 'r')
    for line in fin:
        cols=line.strip().split('\t')
        if cols[0] not in idict: idict[cols[0]]=[]
        idict[cols[0]].append(cols[7])
    fin.close()
    return idict

###-------------------------------------------------------

def paired_prinseq(opts):
    out_name = '%s_good' % '.'.join(opts.paired1.split('/')[-1].split('.')[:-1])
    cmd='%s -fastq %s -fastq2 %s -out_good %s -out_bad null -min_len %d -lc_method dust -lc_threshold 3 -derep 14 -trim_qual_right %d -trim_qual_type %s -trim_qual_window %d -min_qual_mean %d' % (opts.ps, opts.paired1, opts.paired2, out_name, opts.minLength, opts.trimQual, opts.trimType, opts.trimWin, opts.meanQual)
    print(cmd)
    ps=Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
    ps.wait()
    for line in ps.stderr:
        print(line.rstrip())
    
    #Deleting singleton files because they are not being used
    delete_files('%s_1_singletons.fastq' % out_name, '%s_2_singletons.fastq' % out_name)
    
    return '%s_1.fastq' % out_name, '%s_2.fastq' % out_name
    
#For sifting through fastqs and just keeping what is still paired

def paired_sort(read1, read2):
    names1 = get_names(read1)
    names2 = get_names(read2)
    paired = set(names1) & set(names2)

    del names1
    del names2

    pair1_file = write_new_file(read1, paired)
    pair2_file = write_new_file(read2, paired)
    
    return pair1_file, pair2_file

def get_names(file):
    fin = open(file, 'r')       
    names=[]
    linenum=0

    for line in fin:
        linenum+=1
        #First name line
        if linenum%4==1:
            names.append(line.strip().split()[0])
    fin.close()
    return names

def write_new_file(fastq, paired_names):

    fin = open(fastq, 'r')
    fout_pair = open('.'.join(fastq.split('.')[:-1]) + '_paired.fastq', 'w')
    linenum=0
    is_paired=0

    for line in fin:
        linenum+=1
        #First name line
        if linenum%4==1:
            name=line.strip().split()[0]
            if name in paired_names:
                is_paired=1
                fout_pair.write(line)   
            else:
                is_paired=0
        #Other lines
        else:
            if is_paired: fout_pair.write(line)
    fin.close()
    fout_pair.close()
    return '.'.join(fastq.split('.')[:-1]) + '_paired.fastq'


###----------Start of samtools functions----------------------------------------

def sam_to_bam(sam, opts):
    sam_prefix='.'.join(sam.split('/')[-1].split('.')[:-1])
    bam_name='%s.bam' % sam_prefix
    cmd='samtools view --threads %d -bS %s >%s' % (opts.procs, sam, bam_name)
    print(cmd)
    make_bam=Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
    make_bam.wait()
    

def make_sort_bam_pileup(sam, ref, opts):
    #Make new faidx for current reference
    cmd='samtools faidx %s' % ref
    print(cmd)
    faidx=Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
    faidx.wait()
    
    #Make bam from sam, and sort the bam
    sam_prefix='.'.join(sam.split('/')[-1].split('.')[:-1])
    bam_name='%s_sorted.bam' % sam_prefix
    cmd='samtools sort -n -l 0 --threads %d %s | samtools fixmate -m --threads %d - - | samtools sort --threads %d -m 800000000 -o %s' % (opts.procs, sam, opts.procs, opts.procs , sam_prefix + '_sorted.bam')
    print(cmd)
    make_bam=Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
    make_bam.wait()
    
    #Make a new version of the bam with duplicates removed using the markup function in samtools
    if not opts.NOpcrDedup:
        #if os.path.isfile(ext_path + "/picard"):
        #    opts.pc = ext_path + "/picard";
        dup_bam_name='%s_sorted_nodups.bam' % sam_prefix
        #dup_cmd="%s MarkDuplicates REMOVE_DUPLICATES=true ASSUME_SORTED=true O=%s M=%s_metrics.txt I=%s" % (opts.pc, dup_bam_name, dup_bam_name, bam_name)
        dup_cmd="samtools markdup --threads %d -r -s %s %s 2> %s_metrics.txt " % (opts.procs, bam_name, dup_bam_name, dup_bam_name)
        print(dup_cmd)
        rmdups=Popen(dup_cmd, shell=True, stdout=PIPE, stderr=PIPE)
        rmdups.communicate()
        bam_name=dup_bam_name

    
    #Make pileup from bam
    #I added the -B to prevent the altering of the quality values
    pile_name='%s.pile' % sam_prefix
    mpileup_opts=''
    if opts.useAnomPairs: mpileup_opts = mpileup_opts + '-A '
    if opts.disableBAQ:  mpileup_opts = mpileup_opts + '-B '
    cmd='samtools mpileup -a -f %s -q %d -Q %d -d %d %s %s >%s' % (ref, opts.mapQ, opts.baseQual, opts.maxCov, mpileup_opts, bam_name, pile_name)
    print(cmd)
    make_pileup=Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
    make_pileup.wait()
    
    return bam_name, pile_name
#***End of NEW ***


def sort_bam_pileup(bam, ref, opts):
    #Make new faidx for current reference
    cmd='samtools faidx %s' % ref
    print(cmd)
    faidx=Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
    faidx.wait()
    
    #Sort the bam
    bam_prefix='.'.join(bam.split('/')[-1].split('.')[:-1])
    bam_name='%s_sorted.bam' % bam_prefix
    cmd='samtools sort -n -l 0 --threads %d %s | samtools fixmate -m --threads %d - - | samtools sort --threads %d -m 800000000 -o %s' % (opts.procs, bam, opts.procs, opts.procs, bam_prefix + '_sorted.bam')
    print(cmd)
    make_bam=Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
    make_bam.wait()
    
    #Make a new version of the bam with duplicates removed using the markdup function in samtools
    if not opts.NOpcrDedup:
        #if os.path.isfile(ext_path + "/picard"):
        #    opts.pc = ext_path + "/picard";
        dup_bam_name='%s_sorted_nodups.bam' % bam_prefix
        #dup_cmd="%s MarkDuplicates REMOVE_DUPLICATES=true ASSUME_SORTED=true O=%s M=%s_metrics.txt I=%s" % (opts.pc, dup_bam_name, dup_bam_name, bam_name)
        dup_cmd="samtools markdup -r -s --threads %d %s %s 2> %s_metrics.txt " % (opts.procs, bam_name, dup_bam_name, dup_bam_name)
        print(dup_cmd)
        rmdups=Popen(dup_cmd, shell=True, stdout=PIPE, stderr=PIPE)
        rmdups.communicate()
        bam_name = dup_bam_name

#*** Also needed to change the bam being used in the pileup formation
    
    #Make pileup from bam
    pile_name='%s.pile' % bam_prefix
    mpileup_opts=''
    if opts.useAnomPairs: mpileup_opts = mpileup_opts + '-A '
    if opts.disableBAQ:  mpileup_opts = mpileup_opts + '-B '
    cmd='samtools mpileup -a -f %s -q %d -Q %d -d %d %s %s >%s' % (ref, opts.mapQ, opts.baseQual, opts.maxCov, mpileup_opts, bam_name, pile_name)
    print(cmd)
    make_pileup=Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
    make_pileup.wait()
    
    return bam_name, pile_name
#***End of NEW ***


#This function doesn't actually call samtools, but its related
#Creates a new samfile containing only the header and the information for reads that actually mapped to the referene.
#Uses bit 4 of the flag to check if the read is mapped. 
def sam_just_mapped(samfile):
    sam = open(samfile, 'r')
    new_name='%s_onlymapped.sam' % '.'.join(samfile.split('.')[:-1])
    new = open(new_name, 'w')
    for line in sam:
        if line[0] != '@':
            cols=line.split()
            if not int(cols[1]) & 0x4:new.write(line) 
        else: new.write(line)
    sam.close()
    new.close()
    return new_name
    
###----------End of samtools functions----------------------------------------          

###-----------Start of functions used in making consensus from pileup-----------

#Doesn't actually return anything, just changes zero coverage bases to small case Ns
def zero_to_Ns(ref_dict, pileup):
    #Make dict of bases in pileup
    pile_dict={}
    fin=open(pileup, 'r')
    for line in fin:
        cols=line.strip().split('\t')
        if cols[0] not in pile_dict: pile_dict[cols[0]]=[]
        pile_dict[cols[0]].append(int(cols[1]))
    fin.close()

    #Find missing bases and replace them with Ns
    for chrom, base_list in ref_dict.items():
        if chrom in pile_dict:
            missing_bases = set(range(1,len(base_list)+1))-set(pile_dict[chrom])
            for each in missing_bases: ref_dict[chrom][each-1]='n'
        #If the whole chrom has no coverage
        else:
            ref_dict[chrom]= ['n']*len(ref_dict[chrom])

    return ref_dict

def output_gap(changelog, gap_file):
    fout=open(gap_file, 'w')
    fout.write("Start\tEnd\tLength\tRef_ID\n")
    gap_list=list()
    with open(changelog, 'r') as f:
        for line in f:
            if line.startswith("#"):
                continue
            data = line.strip().split('\t')
            pos = int(data[1])
            chrom = data[0]
            if data[3] == 'n':
                if (pos - 1) in gap_list:
                    gap_list.append(pos)
                else:
                    if len(gap_list) == 0:
                        gap_list.append(pos)
                    else:
                        fout.write(f"{gap_list[0]}\t{gap_list[-1]}\t{gap_list[-1] - gap_list[0] + 1}\t{chrom}\n")
                        gap_list=list()
                        gap_list.append(pos)
    if len(gap_list) > 0:
        fout.write(f"{gap_list[0]}\t{gap_list[-1]}\t{gap_list[-1] - gap_list[0] + 1}\t{chrom}\n")
    fout.close()

def correct_changelog(changelog):
    change_log_update = 'change_log_update.changelog'
    fout=open(change_log_update, 'w')
    content=dict()
    with open(changelog, 'r') as f:
        for line in f:
            if line.startswith("#"):
                continue
            data = line.strip().split('\t')
            pos = int(data[1])
            if data[3] == '-':
                if (pos - 1) in content:
                    ref_bases = data[2]
                    # check previous pos has same DP
                    if content[pos-1][4] == data[4]:
                        track_back_pos = pos - 1
                        while 'skip' in content[track_back_pos]:
                            track_back_pos = track_back_pos - 1
                        update_ref_bases = content[track_back_pos][2] + ref_bases
                        content[track_back_pos][2] = update_ref_bases
                        content[pos] = data + ["skip"]
                    else:
                        content[pos] = data
                else:
                    content[pos] = data
    with open(changelog, 'r') as f:
        for line in f:
            data = line.strip().split('\t')
            if line.startswith("#") or data[3] != '-':
                fout.write(line)
            else:
                pos = int(data[1])
                if pos in content and ('skip' not in content[pos]):
                    fout.write('\t'.join(content[pos]) + "\n")
    fout.close()
    os.rename(change_log_update,changelog)
    
def correct_vcf(vcf):
    change_log_vcf_update = 'change_log_update.vcf'
    fvcfout=open(change_log_vcf_update, 'w')
    content=dict()
    with open(vcf, 'r') as f:
        for line in f:
            if line.startswith("#"):
                continue
            if 'INDEL' in line:
                #print(line)
                data = line.strip().split('\t')
                pos = int(data[1])
                if (pos - 1) in content:
                    ref_bases = data[3][1:]
                    if content[pos-1][7] == data[7]:
                        track_back_pos = pos - 1
                        while 'skip' in content[track_back_pos]:
                            track_back_pos = track_back_pos - 1
                        update_ref_bases = content[track_back_pos][3] + ref_bases
                        content[track_back_pos][3] = update_ref_bases
                        content[pos] = data + ["skip"]
                    else:
                        content[pos] = data
                else:     
                    content[pos] = data
                    
    with open(vcf, 'r') as f:
        for line in f:
            if line.startswith("#") or 'INDEL' not in line:
                fvcfout.write(line)
            else:
                data = line.strip().split('\t')
                pos = int(data[1])
                if pos in content and ('skip' not in content[pos]):
                    fvcfout.write('\t'.join(content[pos]) + "\n")
    fvcfout.close()
    os.rename(change_log_vcf_update,vcf)
    
def make_consensus(pileup, ref, opts):
    #Make a dictionary form the reference fasta
    ref_dict=read_fasta_dict_simple_names(ref)
    con_dict={}
    homopolymer={}
    amplicon_dict={}
    if(opts.bed):
        amplicon_dict=convert_bed_to_amplicon_dict(opts.bed)
       
    #Turn each seq into a list 
    for name, seq in sorted(ref_dict.items()):
        if opts.filterHomopolymer:
            homopolymer[name] = find_homopolymer(seq,opts.minHomopolymer)
        ref_dict[name]=list(seq)
        con_dict[name]=list(seq)
    
    #Replace all 0 coverage bases (not included in the pileup) to n  (gap small n)
    con_dict=zero_to_Ns(con_dict, pileup)
    iupac_con_dict=copy.deepcopy(con_dict)
    
    #Open file to append info about changes that are made
    change_log='change_log.txt'
    var_log='var_log.txt'
    composition_log="composition_log.txt"
    change_log_vcf='change_log.vcf'
    gap_file='gaps.txt'
    if opts.comp:
        fcompout=open(composition_log, 'w')
        fcomp_header=('#ID','POS','REF_NUC','TOTAL','A','C','G','T','non-ATCG')
        fcompout.write('\t'.join(fcomp_header) + '\n')
    if not opts.noVcf:
        fvcfout=open(change_log_vcf, 'w')
        vcf_header(fvcfout,pileup,opts,ref_dict)
        vcf_format = "GT:REF_DP:ALT_DP:ALT_FREQ"
    if opts.varlog:
        fout2=open(var_log, 'w')
        log2_header=('#ID','POS','REF_NUC','ALT_NUC','TOTAL_DOC','RATIO_OF_REF','RATIO_OF_ALT','HQ_DOC(Q>{})'.format(opts.baseQual) ,'HQ_RATIO_OF_REF','HQ_RATIO_OF_ALT') 
        fout2.write('\t'.join(log2_header) + '\n')
    fout=open(change_log, 'a')
    log_header=('#ID','POS','REF_NUC','CON_NUC','TOTAL_DOC','RATIO_OF_REF','RATIO_OF_CON','HQ_DOC(Q>{})'.format(opts.baseQual) ,'HQ_RATIO_OF_REF','HQ_RATIO_OF_CON') 
    fout.write('\t'.join(log_header) + '\n')
    num_changes=0                                       #Will keep track of the number of changes that are made to the consensus
    het_changes=0
    previous_indel_info=[]
    #Will hold positions that have been deleted and therefore the pileup positions for these will be skipped
    to_ignore={}
    # hold positions that have deleted pos for varlog
    # to_ignore2={}
        
    fin=open(pileup, 'r')
    current_chrom=''                                    #Need this so that we can reset base_base each time a new contig is encountered
    for line in fin:
        chrom, pos, bases, indels, ends, quals = pileup_info(line)
        #Extract only 'good' quality bases
        good_bases, good_quals = quality_filter(bases, quals, opts)
        cap_bases = [ x.upper() for x in bases ]
        cap_good_bases = [ x.upper() for x in good_bases ] 

        if chrom != current_chrom:
            base_base=-1                                #This value changes with insertions and deletions to keep the position to index relationship correct, it starts at -1 b/c pos is 1-based and indices are 0-based
            current_chrom=chrom

        ref_base=ref_dict[chrom][pos+base_base]
       
        #Change the base at a given position if it is more than the specified % of bases support an alternative
        #Also change to an N if less that opts.covThresh coverage for any particular base 
        if len(good_bases)<1: 
            maj='N'
            het_code=''
            var_cov=0
            strandBias = False
            fscore= 0
            sor = 0
            DP4 = None
        else: maj, het_code, het_cov, het_list, var_code, var_cov, var_list, strandBias, fscore, sor, DP4 = majority_alt(good_bases, ref_base, opts)
        
        ## mask non amplicon region to 'n' if amplicon be provided 
        if amplicon_dict and pos not in amplicon_dict[chrom]:
            maj='n'
            het_code=''
            var_cov=0
            strandBias = False
            fscore= 0
            sor = 0

        ## Deletion
        ## the deletion event is on previous pos pileup 
        ## If there is a deletion after this read base, text matching “-[0-9]+[ACGTNacgtn]+”: a “-” character followed by the deleted reference bases represented similarly.  (Subsequent pileup lines will contain “*” for this read indicating the deleted bases.) (http://www.htslib.org/doc/samtools-mpileup.html)
        ## Not deal with INDELS with het_code case. Current is only based on majority rule.
    
        previous_delete_event_count = 0 if 'delete' not in previous_indel_info else len(previous_indel_info['delete'])
        previous_insert_event_count = 0 if 'insert' not in previous_indel_info else len(previous_indel_info['insert'])
        depth = max(len(bases),len(bases) - bases.count('*') + previous_delete_event_count)
        refFw =  bases.count('.')
        refRv =  bases.count(',') 
        ref_count = refFw + refRv
        good_base_ref_count = good_bases.count('.') + good_bases.count(',')
        inframe_deletion = False
    
        if len(bases) > 0 and bases.count('*')/depth > 0.5 and previous_delete_event_count/depth > 0.5 and 'delete' in previous_indel_info:
            cap_previous_delete_info = [ x.upper() for x in  previous_indel_info['delete'] ]
            max_delete_event = max(cap_previous_delete_info,key=cap_previous_delete_info.count)
            inframe_deletion = True if len(max_delete_event) % 3 == 0 else False
                 
        if  (   
                pos not in to_ignore
                and len(bases) > 0 
                and ( bases.count('*') >= opts.covThresh and bases.count('*')/depth >opts.INDELpropThresh ) 
                or ( len(bases) > 0 and previous_delete_event_count>=opts.covThresh and previous_delete_event_count/depth >opts.INDELpropThresh )
                or ( pos not in to_ignore and inframe_deletion and depth >= opts.covThresh )
            ):
            if 'delete' in previous_indel_info and previous_delete_event_count/depth >opts.INDELpropThresh or inframe_deletion:
                cap_previous_delete_info = [ x.upper() for x in  previous_indel_info['delete'] ]
                max_delete_event = max(cap_previous_delete_info,key=cap_previous_delete_info.count)
            else:
                max_delete_event = ref_base   
            #print(pos, previous_delete_event_count, inframe_deletion, bases.count('*')/depth,max_delete_event,file=sys.stderr)
            max_delete_event_count = 0 if 'delete' not in previous_indel_info else previous_indel_info['delete'].count(max_delete_event.upper()) + previous_indel_info['delete'].count(max_delete_event.lower())
            delete_count = bases.count('*') if bases.count('*') >= max_delete_event_count else max_delete_event_count
            delete_ratio = delete_count/depth
            if opts.filterStrandBias and 'delete' in previous_indel_info:
                del_fscore = fister_exact_test(refFw,refRv,previous_indel_info['delete'].count(max_delete_event.upper()),previous_indel_info['delete'].count(max_delete_event.lower()))
                del_sor = calculate_StrandOddsRatio(refFw,refRv,previous_indel_info['delete'].count(max_delete_event.upper()),previous_indel_info['delete'].count(max_delete_event.lower()))
            #print(pos, previous_delete_event_count, opts.filterHomopolymer, chrom, delete_ratio, opts.hpmINDELpropThresh,bases.count('*')/depth,file=sys.stderr)
            if (opts.filterHomopolymer and pos in homopolymer[chrom] and delete_ratio <= opts.hpmINDELpropThresh):
                #print(pos, previous_delete_event_count, opts.filterHomopolymer, chrom, delete_ratio, opts.hpmINDELpropThresh,bases.count('*')/depth,file=sys.stderr)
                if ref_count < opts.covThresh:
                    pos_to_ignore=pos
                    to_ignore[pos_to_ignore]=1
                    iupac_con_dict[chrom][pos+base_base]='N'
                    con_dict[chrom][pos+base_base]='N'
                    fout.write('%s\t%d\t%s\tN\t%d\t%.4f\t%.4f\t%d\t%.4f\t%.4f\n' % (chrom, pos, ref_base, depth, ref_count/depth, het_cov/depth, len(good_bases), good_base_ref_count/len(good_bases), het_cov/len(good_bases)))
                pass
            elif (opts.filterStrandBias and 'delete' in previous_indel_info and del_fscore > opts.fisherScore and del_sor > opts.strandOddsRatio):
                if ref_count < opts.covThresh:
                    pos_to_ignore=pos
                    to_ignore[pos_to_ignore]=1
                    iupac_con_dict[chrom][pos+base_base]='N'
                    con_dict[chrom][pos+base_base]='N'
                    fout.write('%s\t%d\t%s\tN\t%d\t%.4f\t%.4f\t%d\t%.4f\t%.4f\n' % (chrom, pos, ref_base, depth, ref_count/depth, het_cov/depth, len(good_bases), good_base_ref_count/len(good_bases), het_cov/len(good_bases)))
                pass
            else:
                pos_to_ignore=pos
                to_ignore[pos_to_ignore]=1
                num_changes+=1
                con_dict[chrom][pos+base_base]= '-'
                iupac_con_dict[chrom][pos+base_base]= '-'
            
                #depth = len(bases) if previous_delete_event_count <= bases.count('*') else len(bases) - bases.count('*') + previous_delete_event_count
                
                #if het_code and het_code != ref_base:
                #    fout.write('%s\t%d\t%s\t-(%s)\t%d\t%.4f\t%.4f\n' % (chrom, pos, max_delete_event, het_code, depth, ref_count/depth,het_cov/depth))
                #    fout.write('%s\t%d\t%s\t-(%s)\t%d\t%.4f\t%.4f\n' % (chrom, pos, max_delete_event, het_code, depth, ref_count/depth,het_cov/depth))
                #    iupac_con_dict[chrom][pos+del_base_base]= het_code
                #    het_changes+=1
                #else:
                fout.write('%s\t%d\t%s\t-\t%d\t%.4f\t%.4f\n' % (chrom, pos, max_delete_event, depth, ref_count/depth, delete_ratio))
                if not opts.noVcf:
                    if het_code and het_code != ref_base:
                        fvcfout.write('%s\t%d\t.\t%s\t%s\t.\t.\tINDEL;DP=%d\t%s\t1:%d:%d:%.4f\n' %(chrom, pos - 1, ref_dict[chrom][pos+base_base-1] + max_delete_event, ref_dict[chrom][pos+base_base-1],depth,vcf_format,ref_count,delete_count,delete_ratio))
                        fvcfout.write('%s\t%d\t.\t%s\t%s\t.\t.\tDP=%d\t%s\t1:%d:%d:%.4f\n' %(chrom, pos, ref_dict[chrom][pos+base_base], het_code , depth, vcf_format, ref_count, het_cov, het_cov/depth))
                    else:
                        fvcfout.write('%s\t%d\t.\t%s\t%s\t.\t.\tINDEL;DP=%d\t%s\t1:%d:%d:%.4f\n' %(chrom, pos - 1, ref_dict[chrom][pos+base_base-1] + max_delete_event, ref_dict[chrom][pos+base_base-1],depth,vcf_format,ref_count,delete_count,delete_ratio))
                iupac_con_dict[chrom][pos+base_base]= '-'

                for position in range(1,len(max_delete_event)):
                    pos_to_ignore+=1
                    to_ignore[pos_to_ignore]=1
                    con_dict[chrom][pos+position+base_base]= '-'
                    iupac_con_dict[chrom][pos+position+base_base]= '-'
        
        if opts.varlog:
            if previous_delete_event_count > 0:
                #max_delete_event = max(previous_indel_info['delete'],key=previous_indel_info['delete'].count)
                #pos_to_ignore=pos
                #to_ignore2[pos_to_ignore]=1
                #depth = max(len(bases),len(bases) - bases.count('*') + previous_delete_event_count)
                delete_count = bases.count('*') if bases.count('*') >= previous_delete_event_count else previous_delete_event_count
                delete_ratio = delete_count/depth
                delete_events = []
                cap_delete_info = [ x.upper() for x in previous_indel_info['delete'] ]
                max_delete_event = max(cap_delete_info,key=cap_delete_info.count)
                del_fscore = 0
                del_sor = 0
                if opts.filterStrandBias:
                    del_fscore = fister_exact_test(refFw,refRv,previous_indel_info['delete'].count(max_delete_event.upper()),previous_indel_info['delete'].count(max_delete_event.lower()))
                    del_sor = calculate_StrandOddsRatio(refFw,refRv,previous_indel_info['delete'].count(max_delete_event.upper()),previous_indel_info['delete'].count(max_delete_event.lower()))
                for each in set(cap_delete_info):
                    delete_events.append(each)
                if (opts.filterHomopolymer and pos in homopolymer[chrom]):
                    fout2.write('%s\t%d\t%s\t-\t%d\t%.4f\t%.4f\t%.2f\t%.2f\thomopolymer region\n' % (chrom, pos, ','.join(delete_events), depth, ref_count/depth, delete_ratio,del_fscore,del_sor))
                elif (opts.filterStrandBias and del_fscore > opts.fisherScore and del_sor > opts.strandOddsRatio):
                    fout2.write('%s\t%d\t%s\t-\t%d\t%.4f\t%.4f\t%.2f\t%.2f\tstrand biased\n' % (chrom, pos, ','.join(delete_events), depth, ref_count/depth, delete_ratio,del_fscore,del_sor))
                else:
                    fout2.write('%s\t%d\t%s\t-\t%d\t%.4f\t%.4f\t%.2f\t%.2f\n' % (chrom, pos, ','.join(delete_events), depth, ref_count/depth, delete_ratio,del_fscore,del_sor))
            if var_cov > 0:
                if opts.filterStrandBias and strandBias:
                    fout2.write('%s\t%d\t%s\t%s\t%d\t%.4f\t%.4f\t%d\t%.4f\t%.4f\t%.2f\t%.2f\tstrand biased\n' % (chrom, pos, ref_base, var_code, depth, ref_count/depth, var_cov/depth, len(good_bases), good_base_ref_count/len(good_bases), var_cov/len(good_bases),fscore,sor ))
                else:
                    fout2.write('%s\t%d\t%s\t%s\t%d\t%.4f\t%.4f\t%d\t%.4f\t%.4f\t%.2f\t%.2f\n' % (chrom, pos, ref_base, var_code, depth, ref_count/depth, var_cov/depth, len(good_bases), good_base_ref_count/len(good_bases), var_cov/len(good_bases),fscore,sor))
            if 'insert' in indels: # look ahead
                insert_count = len(indels['insert'])
                insert_ratio = insert_count/depth
                insert_events = []
                cap_insert_info = [ x.upper() for x in indels['insert'] ]
                max_insert_event = max(cap_insert_info,key=cap_insert_info.count)
                #ins_fscore = 0
                #ins_sor = 0
                for each in set(cap_insert_info):
                    insert_events.append(each)
                    event_count = indels['insert'].count(each.upper()) + indels['insert'].count(each.lower())
                    ins_fscore = 0
                    ins_sor = 0
                    if opts.filterStrandBias and max_insert_event == each:
                        ins_fscore = fister_exact_test(refFw,refRv,indels['insert'].count(each.upper()),indels['insert'].count(each.lower()))
                        ins_sor = calculate_StrandOddsRatio(refFw,refRv,indels['insert'].count(each.upper()),indels['insert'].count(each.lower()))
                    if (opts.filterHomopolymer and pos in homopolymer[chrom]):
                        fout2.write('%s\t%d\t-\t%s\t%d\tNA\t%.4f\t%.2f\t%.2f\thomopolymer region\n' % (chrom, pos + 1, each, event_count, event_count/depth, ins_fscore,ins_sor))
                    elif opts.filterStrandBias and ins_fscore > opts.fisherScore and ins_sor > opts.strandOddsRatio:
                        fout2.write('%s\t%d\t-\t%s\t%d\tNA\t%.4f\t%.2f\t%.2f\tstrand biased\n' % (chrom, pos + 1, each, event_count, event_count/depth,ins_fscore,ins_sor))
                    else:
                        fout2.write('%s\t%d\t-\t%s\t%d\tNA\t%.4f\t%.2f\t%.2f\n' % (chrom, pos + 1, each, event_count, event_count/depth,ins_fscore,ins_sor))
                #fout2.write('%s\t%d\t-\t%s\t%d\tNA\t%.4f\n' % (chrom, pos + 1, ','.join(insert_events), insert_count, insert_count/depth))
                
        #Make sure this position has not been deleted
        if pos not in to_ignore:
            if maj and maj != ref_base: 
                if opts.filterStrandBias and strandBias:
                    if ref_count < opts.covThresh:
                        fout.write('%s\t%d\t%s\tN\t%d\t%.4f\t%.4f\t%d\t%.4f\t%.4f\n' % (chrom, pos, ref_base, depth, ref_count/depth, het_cov/depth, len(good_bases), good_base_ref_count/len(good_bases), het_cov/len(good_bases)))
                        con_dict[chrom][pos+base_base]='N'
                        iupac_con_dict[chrom][pos+base_base]='N'
                    pass
                else:
                    con_dict[chrom][pos+base_base]=maj
                    iupac_con_dict[chrom][pos+base_base]=maj
                    num_changes+=1
                
                    if bases == 'n':
                        fout.write('%s\t%d\t%s\t%s\t%d\n' % (chrom, pos,ref_base, bases, 0) )
                        maj='n'
                        con_dict[chrom][pos+base_base]=maj
                        iupac_con_dict[chrom][pos+base_base]=maj
                    elif len(good_bases)==0: fout.write('%s\t%d\t%s\t%s\t%d\t%.4f\t%.4f\t%d\n' % (chrom, pos, ref_base, maj, depth, ref_count/depth, cap_bases.count(maj)/depth, len(good_bases)))
                    else:
                        # there is majority alt code but some other base > hetpropThresh
                        if het_code and het_code != maj:
                            fout.write('%s\t%d\t%s\t%s(%s)\t%d\t%.4f\t%.4f\t%d\t%.4f\t%.4f\n' % (chrom, pos, ref_base, maj, het_code, depth, ref_count/depth, cap_bases.count(maj)/depth, len(good_bases), good_base_ref_count/len(good_bases), cap_good_bases.count(maj)/len(good_bases)))
                            het_changes+=1
                            iupac_con_dict[chrom][pos+base_base]=het_code
                            if not opts.noVcf:
                                fvcfout.write('%s\t%d\t.\t%s\t%s\t.\t.\tDP=%d;DP4=%s\t%s\t1:%d:%d:%.4f\n' %(chrom, pos, ref_base,','.join(het_list),depth,DP4, vcf_format,ref_count,het_cov,het_cov/depth))
                        else:   
                            fout.write('%s\t%d\t%s\t%s\t%d\t%.4f\t%.4f\t%d\t%.4f\t%.4f\n' % (chrom, pos, ref_base, maj, depth, ref_count/depth, cap_bases.count(maj)/depth, len(good_bases), good_base_ref_count/len(good_bases), cap_good_bases.count(maj)/len(good_bases)))
                            if not opts.noVcf and maj != 'N' and maj != 'n':
                                fvcfout.write('%s\t%d\t.\t%s\t%s\t.\t.\tDP=%d;DP4=%s\t%s\t1:%d:%d:%.4f\n' %(chrom, pos, ref_base,maj,depth,DP4, vcf_format,ref_count,cap_bases.count(maj),cap_bases.count(maj)/depth))
            if maj and maj == ref_base:
                if het_code and het_code != ref_base:
                    fout.write('%s\t%d\t%s\t%s(%s)\t%d\t%.4f\t%.4f\t%d\t%.4f\t%.4f\n' % (chrom, pos, ref_base, maj, het_code, depth, ref_count/depth, het_cov/depth, len(good_bases), good_base_ref_count/len(good_bases), het_cov/len(good_bases)))
                    het_changes+=1
                    iupac_con_dict[chrom][pos+base_base]=het_code
                    if not opts.noVcf:
                        fvcfout.write('%s\t%d\t.\t%s\t%s\t.\t.\tDP=%d\t%s\t1:%d:%d:%.4f\n' %(chrom, pos, ref_base,','.join(het_list),depth,vcf_format,ref_count,het_cov,het_cov/depth))
                ## if there are minor INDELs here?
                
            # none nucleotides have depth ratio >=opts.propThresh, consensus base either assign to 'N' or ambiguous code
            if not maj:
                num_changes+=1
                ## Not deletion
                if len(bases)>0 and bases.count('*')/depth<=opts.INDELpropThresh:
                    if het_code:
                        fout.write('%s\t%d\t%s\tN(%s)\t%d\t%.4f\t%.4f\t%d\t%.4f\t%.4f\n' % (chrom, pos, ref_base, het_code, depth, ref_count/depth, het_cov/depth, len(good_bases), good_base_ref_count/len(good_bases), het_cov/len(good_bases)))
                        het_changes+=1
                        con_dict[chrom][pos+base_base]='N'
                        iupac_con_dict[chrom][pos+base_base]=het_code
                        if not opts.noVcf:
                            if het_list:
                                fvcfout.write('%s\t%d\t.\t%s\t%s\t.\t.\tDP=%d\t%s\t1:%d:%d:%.4f\n' %(chrom, pos, ref_base,','.join(het_list),depth,vcf_format,ref_count,het_cov,het_cov/depth))
                            else: #no marjority among A T C G. above opts.hetpropThres. probably INDEL has 25% to 49%
                                pass
                    else:
                        fout.write('%s\t%d\t%s\tN\t%d\t%.4f\t%.4f\t%d\t%.4f\t%.4f\n' % (chrom, pos, ref_base, depth, ref_count/depth, (len(bases) - ref_count)/depth, len(good_bases), good_base_ref_count/len(good_bases), (len(bases) - ref_count)/len(good_bases)))
                        con_dict[chrom][pos+base_base]='N'
                        iupac_con_dict[chrom][pos+base_base]='N'

            #Insert or remove bases if more than the specified % of bases support an indel
            ## Do we need to consider het_code for INDELS? or N for ambiguous           
            ## INSERTION
            if indels:
                
                #if len(indels)>1:
                    #Will only consider the type of structural change with the most support
                #    if len(indels['insert'])>len(indels['delete']): del(indels['delete'])
                #    elif len(indels['delete'])>len(indels['insert']): del(indels['insert'])
                    #If they are equal then neither has a chance of being greater than 50% of the whole
                #    else: 
                #        del(indels['delete'])
                #        del(indels['insert'])
                #If some reads show insertions after this base
                if 'insert' in indels:
                    cap_insert_info = [ x.upper() for x in indels['insert'] ] 
                    for each in set(cap_insert_info):
                        insert_count = indels['insert'].count(each.upper()) + indels['insert'].count(each.lower())
                        if (len(bases)>0 
                            and insert_count/depth>opts.INDELpropThresh and insert_count>=opts.covThresh
                        ):
                            if opts.filterStrandBias:
                                ins_fscore = fister_exact_test(refFw,refRv,indels['insert'].count(each.upper()),indels['insert'].count(each.lower()))
                                ins_sor = calculate_StrandOddsRatio(refFw,refRv,indels['insert'].count(each.upper()),indels['insert'].count(each.lower()))
                            if (opts.filterHomopolymer and pos in homopolymer[chrom] and delete_ratio <= opts.hpmINDELpropThresh):
                                if ref_count < opts.covThresh:
                                    iupac_con_dict[chrom][pos+base_base]='N'
                                    con_dict[chrom][pos+base_base]='N'
                                    fout.write('%s\t%d\t%s\tN\t%d\t%.4f\t%.4f\t%d\t%.4f\t%.4f\n' % (chrom, pos, ref_base, depth, ref_count/depth, het_cov/depth, len(good_bases), good_base_ref_count/len(good_bases), het_cov/len(good_bases)))
                                pass
                            elif opts.filterStrandBias and ins_fscore > opts.fisherScore and ins_sor > opts.strandOddsRatio:
                                if ref_count < opts.covThresh:
                                    iupac_con_dict[chrom][pos+base_base]='N'
                                    con_dict[chrom][pos+base_base]='N'
                                    fout.write('%s\t%d\t%s\tN\t%d\t%.4f\t%.4f\t%d\t%.4f\t%.4f\n' % (chrom, pos, ref_base, depth, ref_count/depth, het_cov/depth, len(good_bases), good_base_ref_count/len(good_bases), het_cov/len(good_bases)))
                                pass
                            else:
                                num_changes+=1
                                #!!!!!This output has not been updated to match the changes made in the SNP output
                                fout.write('%s\t%d\t-\t%s\t%d\tNA\t%.4f\n' % (chrom, pos + 1, each, insert_count, insert_count/depth))
                                if not opts.noVcf:
                                    fvcfout.write('%s\t%d\t.\t%s\t%s\t.\t.\tINDEL;DP=%d\t%s\t1:%d:%d:%.4f\n' %(chrom, pos, ref_dict[chrom][pos+base_base], ref_dict[chrom][pos+base_base] + each, depth ,vcf_format,len(bases)-ends,insert_count,insert_count/depth))
                                iupac_con_dict[chrom].insert((pos+base_base+1), each)
                                con_dict[chrom].insert((pos+base_base+1), each)
                                ref_dict[chrom].insert((pos+base_base+1), '-')
                                base_base+=1                                                        #Need to increment this because all subsequent indices have been shifted forwards
                                break    
                            
        previous_indel_info = indels
        
        if opts.comp:
            #depth = len(bases) if previous_delete_event_count <= bases.count('*') else len(bases) -  bases.count('*') + previous_delete_event_count 
            other_count = depth  - cap_bases.count('G') - cap_bases.count('A') - cap_bases.count('C') - cap_bases.count('T') - ref_count
            if bases == 'n':
                fcompout.write('%s\t%d\t%s\t%d' % (chrom, pos, ref_base, 0))
                other_count = 0
            else:
                fcompout.write('%s\t%d\t%s\t%d' % (chrom, pos, ref_base, depth))
            for b in ['A', 'C', 'G', 'T']:
                if ref_base == b:
                    fcompout.write('\t%d (%.4f)' % (ref_count,ref_count/depth))
                else:
                    fcompout.write('\t%d (%.4f)' % (cap_bases.count(b),cap_bases.count(b)/depth))
            fcompout.write('\t%d (%.4f)\n' % (other_count, other_count/depth))

    fin.close()
    fout.close()
    if not opts.noVcf:
        fvcfout.close()
        correct_vcf(change_log_vcf)
    if opts.comp:
         fcompout.close()
    
    correct_changelog(change_log)
    output_gap(change_log,gap_file)
    
    #Turn each seq into a string
    for name, seq in ref_dict.items():
        ref_dict[name]=''.join(seq)
        con_dict[name]=''.join(con_dict[name])
        iupac_con_dict[name]=''.join(iupac_con_dict[name])
    
    html_outdir = "%s/%s_html" % (opts.startDir, opts.out)
    mkdir_p(html_outdir)
    
    for name, seq in ref_dict.items():
        write_msa_view([name,"%s_%s" % (opts.out,name)],[ref_dict[name],con_dict[name]],"%s/%s.%s" % (html_outdir,name,'html') )
  
    #Write new consensus fasta file
    new_cons_name = 'new_consensus.fasta'
    write_fasta(["%s_%s" % (opts.out ,x) for x in con_dict.keys()], list(con_dict.values()), new_cons_name)

    #Write new consensus with het fasta file
    new_amb_cons_name=''
    if het_changes > 0:
        print("Output new_consensus_w_ambiguous which has %d ambiguous nucleotide code." % het_changes)
        new_amb_cons_name = 'new_consensus_w_ambiguous.fasta'
        write_fasta(["%s_%s" % (opts.out ,x) for x in iupac_con_dict.keys()], list(iupac_con_dict.values()), new_amb_cons_name)
    else:
        print("Zero ambiguous nucleotide code. Will not output consensus_w_ambiguous fasta file.")
    
      
    return num_changes, new_cons_name, new_amb_cons_name
          
def vcf_header(fileHandler,filename,opts,ref_dict):
    header = (
        "##fileformat=VCFv4.2\n"
        "##source=reference-based_assembly_v" + __version__ + "\n"
        "##command=" + ' '.join(sys.argv[1:]) + "\n"
        "##reference=" + opts.ref + "\n"
    )
    for name, seq in ref_dict.items():
        header += ("##contig=<ID=" + name + ",length=" + str(len(seq)) + ">\n")
    header += (
        '##ALT=<ID=*,Description="Represents allele(s) other than observed.">\n'
        '##INFO=<ID=DP,Number=1,Type=Integer,Description="Total Depth">\n'
        '##INFO=<ID=DP4,Number=4,Type=Integer,Description="Depth for Forward Ref Counts, Reverse Ref Counts, Forward Alt Counts, Reverse Alt Counts">\n'
        '##INFO=<ID=INDEL,Number=0,Type=Flag,Description="Indicates that the variant is an INDEL.">\n'
        '##FILTER=<ID=PASS,Description="All filters passed">\n'
        '##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">\n'
        '##FORMAT=<ID=REF_DP,Number=1,Type=Integer,Description="Depth of reference base">\n'
        '##FORMAT=<ID=ALT_DP,Number=1,Type=Integer,Description="Depth of alternate base">\n'
        '##FORMAT=<ID=ALT_FREQ,Number=1,Type=String,Description="Frequency of alternate base">\n'
        "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t" + filename + "\n"
    )
    fileHandler.write(header)
              
def majority_alt(bases, refbase, opts):
    het_dict={
        'A':'A',
        'T':'T',
        'C':'C',
        'G':'G',
        'AC':'M',
        'AG':'R',
        'AT':'W',
        'CG':'S',
        'CT':'Y',
        'GT':'K',
        'CGT':'B',
        'AGT':'D',
        'ACT':'H',
        'ACG':'V',
        'ACGT':'N'
    }
    maj = ''
    het = ''
    var = ''
    var_list = []
    het_list = []
    het_cov = 0
    var_cov = 0
    fscore = 0
    sor = 0
    DP4=''
    capBases = [x.upper() for x in bases]
    refFw =  bases.count('.')
    refRv =  bases.count(',') 
    ref_count = refFw + refRv
    if ref_count/len(bases)>opts.propThresh and ref_count>=opts.covThresh:
        maj=refbase
        
    for b in ['A', 'C', 'G', 'T']:
        base_count = bases.count(b) + bases.count(b.lower())
        if base_count/len(bases)>opts.propThresh and base_count>=opts.covThresh:
            maj=b
        if refbase == b:
            if ref_count>=opts.covThresh and ref_count/len(bases)>opts.hetpropThresh:
                het=het + b
                var=var + b
        else:
            if base_count/len(bases)>opts.hetpropThresh and base_count>=opts.covThresh:
                # minor variants
                het_list.append(b)
                het=het + b
                het_cov = het_cov + base_count
            if base_count > 0:
                var_list.append(b)
                var=var + b
                var_cov = var_cov + base_count
            
    het_code = het_dict[het] if het in het_dict.keys() else ''
    var_code = het_dict[var] if var in het_dict.keys() else ''
    strandBias = False 
    if maj and maj != refbase:
        altFw = bases.count(maj)
        altRv = bases.count(maj.lower())
        fscore = fister_exact_test(refFw,refRv,altFw,altRv)
        sor = calculate_StrandOddsRatio(refFw,refRv,altFw,altRv)
        DP4 = ",".join([str(refFw),str(refRv),str(altFw),str(altRv)])
        #print(DP4,fscore,sor)
        if (fscore > opts.fisherScore and sor > opts.strandOddsRatio): 
            strandBias = True
    #if maj:
    #    return maj, het_code
    # no majority alt change
    #if het_code:
    #    return False, het_code
    if maj or het_code:
        return maj, het_code, het_cov, het_list, var_code, var_cov, var_list, strandBias, fscore, sor, DP4
    else:
        #If none of the alts are above the propThresh and no het_code
        if ref_count<opts.covThresh: 
            return 'N', het_code, het_cov, het_list, var_code, var_cov, var_list, strandBias, fscore, sor,DP4
        else:
            return maj, het_code, het_cov, het_list, var_code, var_cov, var_list, strandBias, fscore, sor,DP4
            
def fister_exact_test(refFw, refRv, altFw, altRv):
    fisher_test_data= [[refFw, refRv],[altFw, altRv]]
    oddsration,pvalue = stats.fisher_exact(fisher_test_data)
    if pvalue:
        score = -10 * np.log10(pvalue) if pvalue < 1.0 else 0
    else:
        print('No Pvalue', refFw, refRv, altFw, altRv)
        score = 10000
    return score
    
def calculate_StrandOddsRatio(refFw, refRv, altFw, altRv):
    # To avoid multiplying or dividing by zero values,
    rFw = refFw + 1
    rRv = refRv + 1
    aFw = altFw + 1
    aRv = altRv + 1
    symmetricalRatio = (rFw * aRv)/(aFw * rRv) + (aFw * rRv)/(rFw * aRv)
    refRatio = min(rFw,rRv)/max(rFw,rRv)
    altRatio = min(aFw,aRv)/max(aFw,aRv)
    SOR = np.log(symmetricalRatio) + np.log(refRatio) - np.log(altRatio)
    return SOR

def quality_filter(bases, quals, opts):
    new_bases=[]
    new_quals=[]
    for index in range(len(bases)):
        if ord(quals[index])-opts.offset >= opts.baseQual:
            new_bases.append(bases[index])
            new_quals.append(quals[index])
    return new_bases, new_quals


def pileup_info(line):
    cols=line.strip().split('\t')
    chrom=cols[0]
    pos=int(cols[1])
    num_reads=int(cols[3])
    if num_reads == 0:
        return chrom, pos, 'n', {}, 0, '#'

    bases, indels, ends = parse_bases(cols[4], num_reads)
    quals=cols[5]
    return chrom, pos, bases, indels, ends, quals

def parse_bases(raw_bases, num_reads):
    #If any reads begin at this position, remove these markers and the associated mapping quality info
    if '^' in raw_bases:
        bases=remove_beg_info(raw_bases)
    else: bases=raw_bases
    #Count the number of reads ending and then remove the $ charcters
    ends=bases.count('$')
    bases=bases.replace('$', '')
    
    #Get info about indels following this position and remove this info form bases
    indels={}           #This line is needed to ensure an empty dict named indels for strings without indel present
    if '-' in bases or '+' in bases:
        bases, indels = indel_info(bases, indels)
    
    if len(bases)==num_reads: return bases, indels, ends
    else:
        print('Problem!!! Exp bases: %d, Output bases: %d, Input str: %s, Output str: %s') % (num_reads, len(bases), raw_bases, bases)
        return #This will cause script to stop because three arguments are expected to be returned      

def remove_beg_info(bases):
    while '^' in bases:
        index=bases.index('^')
        bases=bases[:index]+bases[index+2:]
    return bases

def indel_info(bases, ind_dict):
    ind_dict={}
    if '-' in bases:
        del_info=[]
        while '-' in bases:
            bases, del_info = characterize_indel(bases, '-', del_info)
        ind_dict['delete']=del_info
        #bases=new_bases                
    if '+' in bases:
        ins_info=[]
        while '+' in bases:
            bases, ins_info = characterize_indel(bases, '+', ins_info)
        ind_dict['insert']=ins_info
        #bases=new_bases
    return bases, ind_dict

def characterize_indel(bases, type_char, ind_info):
#    print bases
    start_index=bases.index(type_char)
    digitcount=0
    for each in bases[start_index+1:]:
        if each.isdigit(): digitcount+=1
        else: break
    indel_bases=int(bases[start_index+1:start_index+1+digitcount])
#    print indel_bases
    indel_str=bases[start_index+1+digitcount:start_index+1+digitcount+indel_bases]
#    print indel_str
        
    ind_info.append(indel_str)
    bases=bases[:start_index]+bases[start_index+1+digitcount+indel_bases:]   
    #Recursively go through this process if the string has more indel info
    #if type_char in bases: bases, ind_info = characterize_indel(bases, type_char, ind_info)
    
    return bases, ind_info

# will cut fasta name off at the first whitespace
def read_fasta_lists_simple_names(file):
    fin = open(file, 'r')
    count=0

    names=[]
    seqs=[]
    seq=''
    for line in fin:
        line=line.strip()
        if line and line[0] == '>':                #indicates the name of the sequence
            count+=1
            names.append(line[1:].split()[0])
            if count>1:
                seqs.append(seq)
            seq=''
        else: seq +=line
    seqs.append(seq)

    return names, seqs

def find_homopolymer(seq,minlen):
    """ given seq and min len, return dictionary of each pos in the matched homopolymer"""
    homopolymer={}
    for nucl in ['A','C','T','G']:
        pattern = re.compile(nucl + "{%d,}" % minlen)
        for match in pattern.finditer(str(seq).upper()):
            for i in range(match.start(),match.end()):
                homopolymer[i+1] = match.end() - match.start() 
            #print("\t".join([str(match.start()), str(match.end()), str(match.end() - match.start())  ]))
            
    return homopolymer
            
#writes a new fasta file
def write_fasta(names, seqs, new_filename):
    fout=open(new_filename, 'w')
    for i in range(len(names)):
        seqs[i]=re.sub('-','',seqs[i])
        wrap_seq = re.sub("(.{60})", "\\1\n", seqs[i], 0, re.DOTALL)
        fout.write(">%s\n%s\n" % (names[i], wrap_seq))
    fout.close()

def write_msa_view(names, seqs, new_filename):
    size_limit=500000
    fout=open(new_filename, 'w')
    aln=''
    for i in range(len(names)):
        if len(seqs[i]) > size_limit:
            wrap_seq = re.sub("(.{60})", "\\1\n", seqs[i], 0, re.DOTALL)
            aln += ">%s\n%s\n" % (names[i], wrap_seq)
        else:
            aln += ">%s\\n\\\n%s\\n\\\n" % (names[i], seqs[i])

    html_content=""
    if len(seqs[0]) > size_limit :
        html_content="""<!DOCTYPE html>
<html>
<pre>%s</pre>
</html>""" % aln
    else:
        html_content="""<!DOCTYPE html>
<html>
<head>
  <meta name='description' content='MSAViewer'>
  <meta charset='utf-8'>
  <meta name='viewport' content='width=device-width'>
  <script src='https://cdn.bio.sh/msa/latest/msa.min.gz.js'></script>
  <title>Consensus alignment viewer</title>
</head>
<body>
<div id='msa'>Loading Multiple Alignment...</div>

<script>
var fasta = "%s";

var seqs = msa.io.fasta.parse(fasta);

var m = msa({
    el: document.getElementById('msa'),
    colorscheme: {"scheme": 'nucleotide'},
    seqs: seqs,
    vis: {
        scaleslider: true,
        labelId: false
    },
    zoomer:{
        labelFontsize: 12,
    }
});
m.render();

</script>
</body>
</html>""" % aln
    
    fout.write(html_content)
    fout.close()

def read_fasta_dict_simple_names(file):
    names, seqs = read_fasta_lists_simple_names(file)
    fasta_dict = dict(zip(names, seqs))
    return fasta_dict

###------------END of functions used in building new consensus from pileup----------------------------

###----------Start of bowtie2 functions----------------------------------------

##!!! Expecting fastq quality scores to be 33-offset
def bowtie2_index_align(ref, opts):
    
    #Make index for reference
    cmd='bowtie2-build %s ref_index' % (ref)
    print(cmd)
    build_index=Popen(cmd,shell=True, stdout=PIPE, stderr=PIPE)
    build_index.wait()
    
    sam_name='%s_aligned.sam' % opts.out
    
    #Align unpaired reads
    if opts.unpaired and opts.paired1 and opts.paired2:
        cmd='bowtie2 -p %d --phred33 -N 1 --n-ceil %s -x ref_index -U %s -1 %s -2 %s -S %s' % (opts.procs, opts.nceil, opts.unpaired, opts.paired1, opts.paired2, sam_name)
        print(cmd)
        unpaired_bowtie=Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
        unpaired_bowtie.wait()
    elif opts.unpaired:
        cmd='bowtie2 -p %d --phred33 -N 1 --n-ceil %s -x ref_index -U %s -S %s' % (opts.procs, opts.nceil, opts.unpaired, sam_name)
        print(cmd)
        unpaired_bowtie=Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
        unpaired_bowtie.wait()
    #Align paired reads    
    elif opts.paired1 and opts.paired2:
        cmd='bowtie2 -p %d --phred33 -N 1 -I %d -X %d --n-ceil %s -x ref_index -1 %s -2 %s -S %s' % (opts.procs, opts.minIns, opts.maxIns, opts.nceil, opts.paired1, opts.paired2, sam_name)
        print(cmd)
        paired_bowtie=Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
        paired_bowtie.wait()
    else: 
        print('Must provide at least 1) an unpaired .fastq file (-u) or 2) two paired end fastq files (-1 and -2)!!!!')
        return
    
    return sam_name

###----------End of bowtie2 functions----------------------------------------

        
        
###------------->>>

if __name__ == "__main__":
    main()
