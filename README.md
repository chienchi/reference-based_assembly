# reference-based_assembly


## Installation
Download the latest version of reference-based_assembly from [gitlab](https://gitlab.com/chienchi/reference-based_assembly) or use `git clone` from command line.

```
git clone https://gitlab.com/chienchi/reference-based_assembly
```

`cd` into the `reference-based_assembly` directory

```
cd reference-based_assembly
./INSTALL.sh 
```
The `sudo` privileges are not needed for installation. The installation script will pull required dependencies with internet. A log of all installation can be found in `install.log`

##Dependencies
reference-based_assembly run requires following dependencies which should be in your path. All of the dependencies will be installed by `INSTALL.sh`.

### Programming/Scripting languages
- [Python >=v3.7](https://www.python.org/)
    - The pipeline has only been tested in v3.7.6

### Python packages
- [Biopython (>=1.70)] (http://biopython.org/)
- [NumPy (>=1.10.4)](http://www.numpy.org/) 


### Third party softwares/packages
- [bowtie2 (2.3.4)](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml) - reads aligner
- [samtools (v1.9)](http://www.htslib.org) - process bwa output
- [PRINSEQ (0.20.4)](http://prinseq.sourceforge.net/) - Data QC
- [cutadapt (2.9)](https://github.com/marcelm/cutadapt) - Remove adapter sequences


## Usage
```
Usage: consensus_fasta.py [options] bam1 [bam2 ...]

Options:
  -h, --help            show this help message and exit
  -i INPUT, --input=INPUT
                        Input file for running multiple samples from fastqs
                        [None]
  -u UNPAIRED, --unpaired=UNPAIRED
                        Fastq file with unpaired reads to be mapped to
                        reference [None]
  -1 PAIRED1, --paired1=PAIRED1
                        Fastq file with read 1 of paired reads [None]
  -2 PAIRED2, --paired2=PAIRED2
                        Fastq file with read 2 of paired reads [None]
  -r REF, --ref=REF     Fasta file that was used as reference in mapping.
                        [None, Required]
  -o OUT, --out=OUT     Base name for output files. Output files will be
                        written to the current working directory.
                        [new_consensus]
  -s SAM, --sam=SAM     Optional starting place [None]
  -b BAM, --bam=BAM     Optional starting place [None]
  -p PILE, --pile=PILE  Optional starting place [None]
  --recursThresh=RECURSTHRESH
                        Minimum level of coverage required to change
                        consensus. [3000]
  --temp=TEMP           Name for working directory that will hold internediate
                        files. This is not currently being cleaned [based on
                        -o]
  --procs=PROCS         Number of processors to use in multi-threaded portions
                        [2]
  --Nbeg=NBEG           # of Ns to add to the beginning of each contig [0]
  --Nend=NEND           # of Ns to add to the beginning of each contig [0]
  --mapQ=MAPQ           Minimum mapping quality for a read to be used in the
                        pileup generation [10]
  --maxCov=MAXCOV       Max per base coverage to be used in the pileup
                        generation [300]
  --NOonlyMapped        Use this flag to turn off the part of the script that
                        creates a smaller sam with only mapped reads prior to
                        sorting
  --nceil=NCEIL         Function to supply as --n-ceil parameter in bowtie2.
                        [0,0.3]
  --minIns=MININS       Minimum insert size for a pair to be concordant [0]
  --maxIns=MAXINS       Maximum insert size for a pair to be concordant [500]
  --propThresh=PROPTHRESH
                        Requires that a greater proportion of the reads (than
                        this threshold) must support an alternative for the
                        consensus to be changed. [0.5]
  --covThresh=COVTHRESH
                        Minimum level of coverage required to avoid an N in
                        the consensus. [5]
  --offset=OFFSET       Base quality offset used in the pileup. I believe the
                        default in samtools is Sanger(33) [33]
  --baseQual=BASEQUAL   Minimum base quality for a base to be used in the
                        evalution of the consensus. [20]
  --ca=CA               How to call cutadapt. Need to use version >=1.2
                        [cutadapt]
  --ps=PS               How to call prinseq. [prinseq-lite-0.20.3.pl]
  --i1=I1               Illumina adaptor to be trimmed from the 3' end of R1.
                        [GATCGGAAGAGCACACGTCTGAACTCCAGTCAC]
  --i2=I2               Reverse complement of the Illumina adaptor that will
                        be trimmed from the 3; end of R2
                        [AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT
  -l MINLENGTH, --minLength=MINLENGTH
                        Minimum length to keep (after primer clipping) [70]
  --trimQual=TRIMQUAL   Minimum quality to keep at 3 prime end f seqs [30]
  --trimType=TRIMTYPE   Type of quality score calculation to use. Allowed
                        options are min, mean, max and sum [min]
  --trimWin=TRIMWIN     Size of window to use for quality trimming [5]
  -q MEANQUAL, --meanQual=MEANQUAL
                        Minimum mean quality for a read to be kept [20]
  --sispa=SISPA         For Sispa , use GCCGGAGCTCTGCAGATATC. Add
                        GCCGGAGCTCTGCAGATATCGGCCATTATGGCCGGG and
                        GCCGGAGCTCTGCAGATATCAAAAAAAAAAAAA for Sispa RACE. For
                        kikwit amplicons, add TGTAAAACGACGGCCAGT. Sispa
                        adapter to remove from the seqs. Rev comp of this
                        sequence will be removed from the 3 prime end [None]
  --indexFilt=INDEXFILT
                        Use this to specify the quality cutoff for index
                        filtering. Currently only supported in combination
                        with -i [None].
  --useAnomPairs        Use this flag to use anomalous read pairs in pileup
                        [False]
  --disableBAQ          Disable BAQ calculation in pileup
                        [False]
  --hc=HC               Number of bases to be hard clipped from the beginning
                        of R1 and end of R2. This will be done with prinseq
                        [6]
  --NOcutadapt          Use this flag to turn off the part of the script that
                        runs cutadapt
  --NOprinseq           Use this flag to turn off the part of the script that
                        runs prinseq
  --NOpcrDedup          Use this flag to turn off the part of the script that
                        runs PCR deduplicates
```

## Test

```
cd test
./runTest.sh
```

## Outputs 

- Consensus sequence file in fasta format
- Consensus generation log and stats (.changelog)
- Temp directory (--temp=)

### .changelog tab-delimited table:

 Column | Description
 -------| ----------------------------------------------------------------------------------------
 1      | Name of reference contig/chromosome
 2      | Reference nucleotide position
 3      | Reference base at this position
 4      | Consensus base at this position
 5      | Total depth of coverage
 6      | Proportion of reads supporting the reference base
 7      | Proportion of reads supporting the consensus base
 8      | High quality depth of coverage (base quality >= that specified with the --baseQual flag)
 9      | Proportion of reads supporting the reference base (high quality)
 10     | Proportion of reads supporting the consensus base  (high quality)

## Removing app

For removal, delete (`rm -rf`) `reference-based_assembly` folder, which will remove any packages that were downloaded in that folder. 

