#!/usr/bin/env bash
set -e
rootdir=$( cd $(dirname $0) ; pwd -P )
tempdir=$rootdir/temp

test_result(){
	Test=$rootdir/Test.changelog
	Expect=$rootdir/expected.changelog
	testName="run test";
	if cmp -s "$Test" "$Expect"
	then
		echo "$testName passed!"
		touch "$tempdir/test.success"
	else
		echo "$testName failed!"
		touch "$tempdir/test.fail"
	fi
}

cd $rootdir
echo "Working Dir: $rootdir";
echo "Running Test ..."

rm -rf $tempdir

$rootdir/../consensus_fasta.py --procs=4 -1 R1.fastq -2 R2.fastq -r reference.fasta -o Test --temp=$tempdir || true

rm -rf $rootdir/*cutadapt* $rootdir/*_ca_info.txt $rootdir/*fai

test_result;
